// Bài 1 Tính tiền lương nhân viên

/*
*đầu vào     
*Lương 1 ngày:100000
*Thông tin về số ngày làm
*Các bước xử lý
*. luong=luong_1_ngay*so_ngay_lam;
*Đầu ra 
*Tính tiền lương nhân viên
*/

var one_day_salary = 100000;
var work_number_date = 5;
var result = one_day_salary * work_number_date;
console.log("salary:", result);


//Bài 2 Tính giá trị trung bình

/*
*đầu vào     
*Thông tin về 5 số thực
*Các bước xử lý
*.gia_tri_trung_binh=(num1+num2+num3+num4+num5)/5;
*. 
*.
*Đầu ra 
*Tính giá trị trung bình của 5 số thực
*/
var num1 = 5;
var num2 = 6;
var num3 = 8;
var num4 = 8;
var num5 = 9;
var result = Math.floor((num1 + num2 + num3 + num4 + num5) / 5);
console.log("average:", result);


//Bài 3 Quy đổi tiền 

/*
*đầu vào     
*Giá usd hiện nay là 23500vnd
*Thông tin về số tiền usd cần đổi sang vnd
*Các bước xử lý
*.tien_quy_doi=23500*so_tien_usd_can_doi;
*. 
*.
*Đầu ra 
*Tính và xuất số tiền sau quy đổi vnd 
*/
var one_usd = 23500;
var user_usd = 5;
var result = one_usd * user_usd;
console.log("VND:", result);


//Bài 4 Tính diện tích, chu vi hình chữ nhật

/*
*đầu vào     
*Thông tin về chiều dài chiều rộng của hình chữ nhật
*Các bước xử lý
*.chu_vi=(chiều dài+chiều rộng)*2;
*. dien_tich=chiều dài *chiều rộng;
*Đầu ra 
*Tính chu vi và diện tích hình chữ nhật
*/
var long = 15;
var width = 13;
var area = long * width;
console.log("area:", area);
var perimeter = (long + width) * 2;
console.log("perimeter:", perimeter);


//Bài 5 Tính tổng hai ký số

/*
*đầu vào     
*Thông tin về số có hai chữ số
*Các bước xử lý
*.hang_don_vi=so%10;
*.hang_chuc=so/10;
*.Tong_2_ky_so=hang_don_vi+hang_chuc;
*Đầu ra 
*Tính tổng hai ký số của số vừa nhập
*/
var num = 45;
var unit = 45 % 10;
var tens = Math.floor(45 / 10);
var result = unit + tens;
console.log("sum:", result);